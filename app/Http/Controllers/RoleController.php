<?php

namespace App\Http\Controllers;

use App\Models\device;
use App\Models\gejala;
use App\Models\role;
use Facade\FlareClient\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = role::all();

        return View('aturan.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = gejala::all();
        $device = device::all();
        return view('aturan.add', compact(['data','device']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $data = $request->validate([
            'kategori' => 'required',
            'pertanyaan1' => 'required',
            'a' => 'required',
            'pertanyaan2' => 'required',
            'b' => 'required',
            'pertanyaan3' => 'required',
            'c' => 'required',
            'pertanyaan4' => 'required',
            'd' => 'required',
            'hasil' => 'required',
        ]);
        // dd($data);
        role::create($data);
        return Redirect()->route('aturan.index');
    }

    public function salin($id){
        $data = role::find($id);
        $gejala = gejala::all();
        return view('aturan.copy', compact(['data','gejala']));
    }
    public function save_salin(Request $request){
        $data = $request->validate([
            'kategori' => 'required',
            'pertanyaan1' => 'required',
            'a' => 'required',
            'pertanyaan2' => 'required',
            'b' => 'required',
            'pertanyaan3' => 'required',
            'c' => 'required',
            'pertanyaan4' => 'required',
            'd' => 'required',
            'hasil' => 'required',
        ]);
        // dd($data);
        role::create($data);
        return Redirect()->route('aturan.index');
    }

    public function destroy($id)
    {
        $data = role::find($id);
        $data->delete();
        return Redirect()->route('aturan.index');
    }
}
