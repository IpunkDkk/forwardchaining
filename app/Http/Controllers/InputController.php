<?php

namespace App\Http\Controllers;

use App\Models\device;
use App\Models\input;
use App\Models\role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class InputController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function verifikasi(){
        $device = device::all();
        return view('konsultasi.verifikasi', compact('device'));
    }
    public function verifikasi2(Request $request){
        $kategori = $request->kategori;
        return Redirect()->route('konsultasi.add', compact('kategori'));
    }

    public function index()
    {
        // dd($request->all());
        $data = input::all();
        return view('konsultasi.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $kategori = $request->kategori;
        // dd($kategori);
        $aturan = role::where('kategori','=',$kategori)->first();
        return view('konsultasi.add',compact('aturan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'a' => 'required',
            'b' => 'required',
            'c' => 'required',
            'd' => 'required',
        ]);
        $p1 = $request->a;
        $p2 = $request->b;
        $p3 = $request->c;
        $p4 = $request->d;
        $aturan = role::where('a',$p1)->where('b',$p2)->where('c',$p3)->where('d',$p4)->get();
        if ($aturan){
            $data['nilai'] = $aturan[0]['hasil'];
            input::create($data);
            return Redirect()->route('konsultasi.index' , compact('aturan'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\input  $input
     * @return \Illuminate\Http\Response
     */
    public function show(input $input)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\input  $input
     * @return \Illuminate\Http\Response
     */
    public function edit(input $input)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\input  $input
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, input $input)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\input  $input
     * @return \Illuminate\Http\Response
     */
    public function destroy(input $input)
    {
        //
    }
}
