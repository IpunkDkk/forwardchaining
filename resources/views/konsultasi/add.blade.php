@extends('template.content')
@section('content')
<div class="container p-5">
    <form action="{{route('konsultasi.add')}}" method="POST">
        @csrf
        <div class="mt-5 mb-3">
            <label for="pertanyaan1" class="form-label"> {{ $aturan->pertanyaan1 }} </label>
            <select name="a" class="form-select form-select-sm" aria-label=".form-select-sm example">
                <option selected>Nilainya</option>
                <option value="1">iya</option>
                <option value="0">tidak</option>
            </select>
          </div>
          <div class="mb-3">
            <label for="pertanyaan2" class="form-label">{{ $aturan->pertanyaan2 }}</label>
            <select name="b" class="form-select form-select-sm" aria-label=".form-select-sm example">
                <option selected>Nilainya</option>
                <option value="1">iya</option>
                <option value="0">tidak</option>
            </select>
          </div>
          <div class="mb-3">
            <label for="pertanyaan3" class="form-label">{{ $aturan->pertanyaan3 }}</label>
            <select name="c" class="form-select form-select-sm" aria-label=".form-select-sm example">
                <option selected>Nilainya</option>
                <option value="1">iya</option>
                <option value="0">tidak</option>
            </select>
          </div>
          <div class="mb-3">
            <label for="pertanyaan4" class="form-label">{{ $aturan->pertanyaan4 }}</label>
            <select name="d" class="form-select form-select-sm" aria-label=".form-select-sm example">
                <option selected>Nilainya</option>
                <option value="1">iya</option>
                <option value="0">tidak</option>
            </select>
          </div>
          <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
</div>
@endsection
