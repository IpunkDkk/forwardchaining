@extends('template.content')
@section('content')
<div class="container p-5">
    <form action="{{route('konsultasi.verif')}}" method="post">
        @csrf
        <div class="mb-3">
            <select name="kategori" class="form-select form-select" aria-label=".form-select-sm example">
              <option selected>Type Devices</option>
              @foreach ($device as $item)
                <option value="{{ $item->type }}">{{ $item->type }}</option>
              @endforeach
            </select>
          </div>
          <button type="submit" class="btn btn-primary">lanjutkan</button>
    </form>
</div>

@endsection