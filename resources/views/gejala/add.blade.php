@extends('template.content')
@section('content')
<div class="container p-5">
    <form action="{{route('gejala.add')}}" method="POST">
        @csrf
        <div class="mt-5 mb-3">
            <label for="gejala" class="form-label">Gejala</label>
            <input type="text" class="form-control" id="gejala" name="gejala">
        </div>
        <button type="submit" class="btn btn-primary">Tambah Gejala</button>
    </form>
</div>
@endsection
