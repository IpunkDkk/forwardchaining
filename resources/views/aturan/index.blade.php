@extends('template.content')
@section('content')
<div class="container">
    <a class="btn btn-primary" role="button" href="{{ route('aturan.add') }}">Tambah Aturan</a>
    <table class="table">
        <thead>
          <tr>
            <th scope="col">NO</th>
            <th scope="col">ID</th>
            <th scope="col">aturan 1</th>
            <th scope="col">aturan 2</th>
            <th scope="col">aturan 3</th>
            <th scope="col">aturan 4</th>
            <th scope="col">nilai</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
        @foreach ($data as $item)    
        <tr>
          <th scope="row">{{ $loop->iteration }}</th>
          <td>{{ $item->id }}</td>
          @if ($item->a)
              <td>iya</td>
          @else
              <td>tidak</td>
          @endif
          @if ($item->b)
              <td>iya</td>
          @else
              <td>tidak</td>
          @endif
          @if ($item->c)
              <td>iya</td>
          @else
              <td>tidak</td>
          @endif
           @if ($item->d)
               <td>iya</td>
            @else
              <td>tidak</td>
          @endif
          <td>{{ $item->hasil }}</td>
          <td>
            <a class="btn btn-info btn-sm" role="button" href="{{ route('aturan.copy', $item->id) }}">Copy</a>
            <form method="POST" action="{{ route('aturan.delete', $item->id) }}" id="hapus">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger btn-sm" type="submit">Hapus</button>
            </form>
          </td>
        </tr>
        @endforeach
        </tbody>
      </table>
</div>
@endsection