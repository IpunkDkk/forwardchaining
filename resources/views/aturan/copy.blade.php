@extends('template.content')
@section('content')
<div class="container p-5">
    <form action="{{route('aturan.copy',0)}}" method="POST">
        @csrf
        <div class="mt-5 mb-3">
            <label for="pertanyaan1" class="form-label">pertanyaan 1</label>
            <input type="text" class="form-control" id="pertanyaan1" name="pertanyaan1" value="{{ $data->pertanyaan1 }}">
            <select name="a" class="form-select form-select-sm" aria-label=".form-select-sm example">
                <option selected>Nilainya</option>
                <option value="1">iya</option>
                <option value="0">tidak</option>
            </select>
          </div>
          <div class="mb-3">
            <label for="pertanyaan2" class="form-label">pertanyaan 2</label>
            <input type="text" class="form-control" id="pertanyaan2" name="pertanyaan2" value="{{ $data->pertanyaan2 }}">
            <select name="b" class="form-select form-select-sm" aria-label=".form-select-sm example">
                <option selected>Nilainya</option>
                <option value="1">iya</option>
                <option value="0">tidak</option>
            </select>
          </div>
          <div class="mb-3">
            <label for="pertanyaan3" class="form-label">pertanyaan 3</label>
            <input type="text" class="form-control" id="pertanyaan3" name="pertanyaan3" value="{{ $data->pertanyaan3 }}">
            <select name="c" class="form-select form-select-sm" aria-label=".form-select-sm example">
                <option selected>Nilainya</option>
                <option value="1">iya</option>
                <option value="0">tidak</option>
            </select>
          </div>
          <div class="mb-3">
            <label for="pertanyaan4" class="form-label">pertanyaan 4</label>
            <input type="text" class="form-control" id="pertanyaan4" name="pertanyaan4" value="{{ $data->pertanyaan4 }}">
            <select name="d" class="form-select form-select-sm" aria-label=".form-select-sm example">
                <option selected>Nilainya</option>
                <option value="1">iya</option>
                <option value="0">tidak</option>
            </select>
          </div>
          <div class="mb-3">
            <select name="hasil" class="form-select form-select-sm" aria-label=".form-select-sm example">
              <option selected>Hasil</option>
              @foreach ($gejala as $item)
                <option value="{{ $item->gejala }}">{{ $item->gejala }}</option>
              @endforeach
            </select>
          </div>
          <button type="submit" class="btn btn-primary">Tambah Aturan</button>
    </form>
</div>
@endsection
