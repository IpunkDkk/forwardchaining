<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->string('kategori');
            $table->string('pertanyaan1');
            $table->enum('a',[1,0])->default(0);
            $table->string('pertanyaan2');
            $table->enum('b',[1,0])->default(0);
            $table->string('pertanyaan3');
            $table->enum('c',[1,0])->default(0);
            $table->string('pertanyaan4');
            $table->enum('d',[1,0])->default(0);
            $table->string('hasil');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
