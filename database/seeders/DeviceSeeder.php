<?php

namespace Database\Seeders;

use App\Models\device;
use Illuminate\Database\Seeder;

class DeviceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $device = new device();
        $device->create([
            'type' => 'handphone',
        ]);
    }
}
