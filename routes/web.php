<?php

use App\Http\Controllers\GejalaController;
use App\Http\Controllers\InputController;
use App\Http\Controllers\RoleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::prefix('konsultasi')->group(function(){
    Route::get('/' , [InputController::class, 'index'])->name('konsultasi.index');
});

Route::prefix('aturan')->group(function(){
    Route::get('/' , [RoleController::class , 'index'])->name('aturan.index');
    Route::get('add' , [RoleController::class , 'create'])->name('aturan.add');
    Route::post('add' , [RoleController::class , 'store'])->name('aturan.add');
    Route::get('copy/{id}' , [RoleController::class , 'salin'])->name('aturan.copy');
    Route::post('copy/{id}' , [RoleController::class , 'save_salin'])->name('aturan.copy');
    Route::delete('delete/{id}' , [RoleController::class , 'destroy'])->name('aturan.delete');
});

Route::prefix('gejala')->group(function(){
    Route::get('/', [GejalaController::class, 'index'])->name('gejala.index');
    Route::get('/add', [GejalaController::class, 'create'])->name('gejala.add');
    Route::post('/add', [GejalaController::class, 'store'])->name('gejala.add');
    Route::delete('/delete/{id}', [GejalaController::class, 'destroy'])->name('gejala.delete');
});

Route::prefix('konsultasi')->group(function(){
    Route::get('/',[InputController::class, 'index'])->name('konsultasi.index');
    Route::get('/verif',[InputController::class, 'verifikasi'])->name('konsultasi.verif');
    Route::post('/verif',[InputController::class, 'verifikasi2'])->name('konsultasi.verif');
    Route::get('/add',[InputController::class, 'create'])->name('konsultasi.add');
    Route::post('/add',[InputController::class, 'store'])->name('konsultasi.add');
});